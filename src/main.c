#include "mem.h"
#include "mem_internals.h"
#include <stdio.h>
#include <sys/mman.h>

#define HEAP_SIZE 4096
#define BLOCK_SIZE_1 100
#define BLOCK_SIZE_2 200
#define BLOCK_SIZE_3 300
#define BLOCK_SIZE_4 10
#define BLOCK_SIZE_5 20
#define BLOCK_SIZE_6 30
#define BLOCK_SIZE_BIGGER_HEAP 5000

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}


static void *map_pages(void const *addr, size_t length, int additional_flags) {
  return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

static void test_malloc(){
    printf("Tест 1: обычное выделение памяти\n");
    void* block = _malloc(BLOCK_SIZE_1);
    struct block_header* block_ = block_get_header(block);
    if (block_ -> is_free != false || block_ -> capacity.bytes != BLOCK_SIZE_1){
        printf("Тест 1: провален\n");
        return;
    }
    _free(block);
    if (block_ -> is_free == false){
        printf("Тест 1: провален\n");
        return;
    }
    printf("Тест 1: пройден\n\n");
}

static void test_free_one_block() {
    printf("Тест 2: Освобождение одного блока из нескольких выделенных\n");

    void* block1 = _malloc(BLOCK_SIZE_1);
    struct block_header* block_1 = block_get_header(block1);

    void* block2 = _malloc(BLOCK_SIZE_2);
    struct block_header* block_2 = block_get_header(block2);

    void* block3 = _malloc(BLOCK_SIZE_3);
    struct block_header* block_3 = block_get_header(block3);

    _free(block1);

    if (block_1 -> is_free != true || block_2 -> is_free != false || block_3 -> is_free != false){
        printf("Тест 2: провален\n");
        return;
    } else {
        printf("Тест 2: пройден\n\n");
    }

    _free(block2);
    _free(block3);
}

static void test_free_two_blocks() {
    printf("Тест 3: Освобождение двух блоков из нескольких выделенных\n");

    void* block1 = _malloc(BLOCK_SIZE_4);
    struct block_header* block_1 = block_get_header(block1);

    void* block2 = _malloc(BLOCK_SIZE_5);
    struct block_header* block_2 = block_get_header(block2);

    void* block3 = _malloc(BLOCK_SIZE_6);
    struct block_header* block_3 = block_get_header(block3);

    _free(block1);
    _free(block2);

    if (block_1 -> is_free != true || block_2 -> is_free != true || block_3 -> is_free != false){
        printf("Тест 3: провален\n");
        return;
    } else {
        printf("Тест 3: пройден\n\n");
    }

    _free(block3);
}

static void new_region_expands(void* heap) {
    printf("Тест 4: новый регион памяти расширяет старый\n");

    //размер блока больше, чем размер кучи
    struct block_header* heap_ = (struct block_header*) heap;
    size_t before_heap_size = heap_ -> capacity.bytes;
    void* new_block = _malloc(BLOCK_SIZE_BIGGER_HEAP);
    struct block_header* new__block = block_get_header(new_block);

    if (new__block -> capacity.bytes != BLOCK_SIZE_BIGGER_HEAP || (before_heap_size >= heap_ -> capacity.bytes)){
        printf("Тест 4: провален\n");
        return;
    }

    printf("Тест 4: пройден\n\n");
    _free(new_block);
}

static void new_region_not_expands() {
    printf("Тест 5: новый регион выделяется в другом месте\n");

    void* before_allocation = map_pages((const void *)HEAP_START, BLOCK_SIZE_4, MAP_FIXED);
    void* after_allocation = _malloc(BLOCK_SIZE_5);

    if(before_allocation == after_allocation){
        printf("Тест 5: провален\n");
        return;
    }
    printf("Тест 5: пройден\n\n");
    _free(after_allocation);
}


int main() {
    void* heap = heap_init(HEAP_SIZE);

    test_malloc();
    test_free_one_block();
    test_free_two_blocks();
    new_region_expands(heap);
    new_region_not_expands();

    heap_term();
}
