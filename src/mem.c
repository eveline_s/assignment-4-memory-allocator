#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
  size_t region_size = region_actual_size(query + offsetof(struct block_header, contents));
    void* address = map_pages(addr, region_size, MAP_FIXED_NOREPLACE);
    if (address == MAP_FAILED){
        address = map_pages(addr, region_size, 0);
        if (address == MAP_FAILED){
            return REGION_INVALID;
        }
    }
    block_init(address, (block_size) {region_size}, NULL);
    struct region new_region = {.addr = address, .size = region_size, .extends = (addr == address)};
    return new_region;
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

static void* block_after( struct block_header const* block )              {
   return  (void*) (block->contents + block->capacity.bytes);
 }
 static bool blocks_continuous (
                                struct block_header const* fst,
                                struct block_header const* snd ) {
   return (void*)snd == block_after(fst);
 }


 /*  освободить всю память, выделенную под кучу */
 void heap_term( ) { 
    struct block_header *block = HEAP_START; 
    struct block_header *next; 
    while(block) { 
        size_t current_size = size_from_capacity(block->capacity).bytes; 
        next = block -> next; 
        while (next != NULL && blocks_continuous(block, next)) { 
            current_size += size_from_capacity(next->capacity).bytes; 
            next = next -> next; 
        } 
        munmap(block, current_size); 
        block = next; 
    } 
}


 #define BLOCK_MIN_CAPACITY 24

 /*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

 static bool block_splittable( struct block_header* restrict block, size_t query) {
   return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
 }

 static bool split_if_too_big( struct block_header* block, size_t query ) {
    if (!block) return false;
    query = size_max(query, BLOCK_MIN_CAPACITY);
    if(!block_splittable(block, query)) return false;

    struct block_header* next_block = (struct block_header*) (block -> contents + query);
    block_size next_block_size = (block_size){size_from_capacity(block->capacity).bytes - size_from_capacity((block_capacity){query}).bytes};

    next_block -> is_free = true;
    next_block -> capacity = capacity_from_size(next_block_size);
    next_block -> next = block -> next;

    block -> next = next_block;
    block -> capacity.bytes = query;

    return true;
 }


 /*  --- Слияние соседних свободных блоков --- */

 
 static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
   return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
 }

 static bool try_merge_with_next( struct block_header* block ) {
   if (block == NULL || block->next == NULL) return false;
   struct block_header* next_block = block_after(block);
     if (!mergeable(block, next_block)) return false;

    block -> next = next_block -> next;
    block -> capacity.bytes += next_block -> capacity.bytes + offsetof(struct block_header, contents);
    return true;
 }


 /*  --- ... ecли размера кучи хватает --- */

 struct block_search_result {
   enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
   struct block_header* block;
 };


 static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
    if (!block){
         return (struct block_search_result) {.block = NULL, .type = BSR_CORRUPTED};
     }

     struct block_header* prev_block;

     while (block != NULL){
       
         while(try_merge_with_next(block)){}
  
         if (block -> is_free && block_is_big_enough(sz, block)){
           return (struct block_search_result) {.block = block, .type = BSR_FOUND_GOOD_BLOCK};
        }
       prev_block = block;
       block = block -> next;
     }
    
     return (struct block_search_result) {.block = prev_block, .type = BSR_REACHED_END_NOT_FOUND};
 }

 /*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
  Можно переиспользовать как только кучу расширили. */
 static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
     query = size_max (query, BLOCK_MIN_CAPACITY);
     struct block_search_result result = find_good_or_last(block, query);

     if (result.type != BSR_FOUND_GOOD_BLOCK){
         return result;
     } else {
         split_if_too_big(result.block, query);
         result.block -> is_free = false;
         return result;
     }
 }



  static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
    if (last == NULL){
        return NULL;
    }
    struct region new_region = alloc_region(block_after(last), query);
    if (region_is_invalid(&new_region)){
        return NULL;
    }
    last -> next = new_region.addr;
    if (new_region.extends && try_merge_with_next(last)){
        return last;
    }
    return new_region.addr;
}

 /*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
 static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  if (query <= 0) return NULL;
     if (!heap_start) return NULL;
     query = size_max(query, BLOCK_MIN_CAPACITY);
     struct block_search_result result = try_memalloc_existing(query, heap_start);

    switch (result.type){
     case BSR_FOUND_GOOD_BLOCK:
         return  result.block;

     case BSR_CORRUPTED:
         return NULL;

     default: {
         struct block_header* heap = grow_heap(result.block, query);
         if (!heap) return NULL;
         result = try_memalloc_existing(query, heap);
         if (result.type == BSR_FOUND_GOOD_BLOCK){
             return result.block;
         } else {
             return NULL;
         }
     }
     }
 }


 void* _malloc( size_t query ) {
   struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
   if (addr) return addr->contents;
   return NULL;
}

 static struct block_header* block_get_header(void* contents) {
   return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
 }

 void _free( void* mem ) {
   if (!mem) return ;
   struct block_header* header = block_get_header( mem );
   header->is_free = true;
   while(try_merge_with_next(header));
}
